package com.spring.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class WelcomeController {
@RequestMapping("/welcome")
	public String welcome()
	{
		return "{\"nam\":\"welcome to this file\"}";
	}
}

